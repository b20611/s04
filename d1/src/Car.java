public class Car {

    //Properties/attributes - the characteristics of the object the class will create.
    //Constructor - method to create the object and instantiate with its initial values
    //Getters and setters - are methods to get values of an object's properties or set them.
    //Methods - actions that an object can perform or do.

    //public access - the variable/property in the class is accessible anywhere in the application.
    //Attributes/properties of a class should not be made public. They should only be accessed with getters and setters instead of just notation.
    //private - limits the access and ability to get or set a variable/method to only within its own class.
    //getters - methods that return the value of the property
    //setters - methods that allow us to set the value of the property
    private String make;

    private String brand;

    private int price;

    private Driver carDriver;

    //Constructor is a method which allows us to set the initial values of an instance
    //empty/default constructor - allows us to create an instance with default initialized values.
    //By default, Java, when your class does not have a constructor, assigns one for you. Java also sets the default values. You could have a way to add your own default values.
    public Car(){
        //this.brand = "Geely";

    }

    public Car(String make,String brand,int price,Driver driver){

        this.make = make;
        this.brand = brand;
        this.price = price;
        this.carDriver = driver;

    }

    //Getters and Setters for our properties
    //Getters return a value so we must add the dataType of the value returned.
    public String getMake(){
        //this keyword refers to the object/instance where the constructor or setter/getter is.
        return this.make;
    }

    public void setMake(String makeParams){
        this.make = makeParams;
    }

    //You could set a property as read-only, this means that the value of the property can only be get but not updated
    public String getBrand(){
        return this.brand;
    }

//    public void setBrand(String brandParams){
//        this.brand = brandParams;
//    }

    public int getPrice(){
        return this.price;
    }

    public void setPrice(int priceParams){
        this.price = priceParams;
    }

    //Methods are functions of an object/instance which allows us to perform certain tasks.
    //void - means that the function does not return anything. Because in Java, a method's return dataType must also be declared.
    public void start(){

        System.out.println("Vrooom! Vrooom!");
    }

    //Classes have relationship
    /*
        Composition allows modelling objects to be made up of other objects. Classes can have instances of other classes.

        A car has a Driver.
    */

    public Driver getCarDriver() {
        return carDriver;
    }

    public void setCarDriver(Driver carDriver) {
        this.carDriver = carDriver;
    }

    //custom method to retrieve the car driver's name:
    public String getCarDriverName(){
        return this.carDriver.getName();
    }
}
